import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from "@angular/material/dialog";
import {UserService} from "../user.service";
import {User} from "../model/user.model";
import {TaskServiceService} from "../task-service.service";

@Component({
  selector: 'app-share',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.css']
})
export class ShareComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private shareWindow: MatDialog, private userService: UserService, private taskService: TaskServiceService) {
  }

  users: User[];

  ngOnInit(): void {
    this.getAll();
  }

  close() {
    this.shareWindow.closeAll();
  }

  getAll() {
    this.userService.allUsers().subscribe(data => {
      this.users = data;
    });
  }

  share(userId: string) {
    this.taskService.share(this.data.id, userId).subscribe();
    this.close();
  }
}
