import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Task} from './model/task.model';


@Injectable({
  providedIn: 'root'
})


export class TaskServiceService {
  constructor(private http: HttpClient) {
  }

  task: Task = new Task();

  getTasks(): Observable<Task[]> {
    console.log("TASK FIND ALL");
    return this.http.get<Task[]>('https://taskcontroller14.herokuapp.com/api/tasks');
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }

  create(task: Task) {
    console.log('[create task]');
    const body = {
      title: task.title,
      description: task.description,
      created: task.created,
    };
    return this.http.post('https://taskcontroller14.herokuapp.com/api/task/create', body).subscribe();
  }

  deleteTask(id: string) {
    return this.http.get('https://taskcontroller14.herokuapp.com/api/task/delete/' + id, {responseType: 'text'});
  }

  update(task: Task) {
    const body = {
      id: task.id,
      title: task.title,
      description: task.description,
    };
    return this.http.post('https://taskcontroller14.herokuapp.com/api/task/update', body).subscribe();
  }


  getCurrentUser() {
    return this.http.get('https://taskcontroller14.herokuapp.com/api/v1/users/current');
  }


  share(taskId, userToId) {
    return this.http.get('https://taskcontroller14.herokuapp.com/api/task/share/' + userToId + '/' + taskId, {responseType: 'text'});
  }

}
