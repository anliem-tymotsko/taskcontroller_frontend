import {Component, OnInit} from '@angular/core';
import {TaskServiceService} from '../task-service.service';
import {AuthService} from '../auth.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Task} from '../model/task.model';
import {MatDialog} from '@angular/material/dialog';
import {UpdateTaskComponent} from '../update-task/update-task.component';
import {timer} from 'rxjs';
import {ShareComponent} from '../share/share.component';
import {User} from "../model/user.model";
import {UserService} from "../user.service";

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  constructor(private userService: UserService, private taskService: TaskServiceService, private authService: AuthService, private router:
    Router, private  formBuilder: FormBuilder, private dialog: MatDialog) {

    this.createTaskForm = this.formBuilder.group({
      taskTitle: '',
      taskDescription: '',
      taskCreated: null,
      user: User
    });
  }

  tasks = [];
  user: any;
  createTaskForm: FormGroup;
  taskTitle: string;
  taskDescription: string;
  task: Task = new Task();
  timerSubscription: any;
  check: any

  ngOnInit(): void {
    this.allTasks();
    this.getCurrentUser();
  }

  allTasks() {
    this.taskService.getTasks().subscribe((data: any) => {
        this.tasks = data;
        this.subscribeToData();
      }
    );
  }

  createTask(taskData) {
    this.task.title = taskData.taskTitle;
    this.task.description = taskData.taskDescription;
    this.task.created = new Date();
    this.taskService.create(this.task);
    this.ngOnInit();
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }

  delete(id: string, i: any) {
    this.taskService.deleteTask(id).subscribe();
    this.tasks.forEach((task) => {
      if (task.id === id) {
        this.tasks.splice(i, 1);
      }
    });

  }

  update(task: Task) {
    this.dialog.open(UpdateTaskComponent, {
      data: {
        id: task.id,
        title: task.title,
        description: task.description,
        created: task.created
      }
    });
  }

  getCurrentUser() {
    this.taskService.getCurrentUser().subscribe((data) => {
      this.user = data;
      console.log(this.user);
    });
  }

  private subscribeToData() {
    this.timerSubscription = timer(5000).subscribe(() => this.allTasks());
  }

  share(taskId: any) {
    this.dialog.open(ShareComponent, {
      data: {
        id: taskId
      }
    });

  }


}
