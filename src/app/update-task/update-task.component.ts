import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {Task} from '../model/task.model';
import {TaskServiceService} from '../task-service.service';
import {FormBuilder, FormGroup} from '@angular/forms';


@Component({
  selector: 'app-update-task',
  templateUrl: './update-task.component.html',
  styleUrls: ['./update-task.component.css'],

})

export class UpdateTaskComponent implements OnInit {

  createTaskForm: FormGroup;
  id: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private dialog: MatDialog, private taskService: TaskServiceService, private  formBuilder: FormBuilder) {
    this.createTaskForm = this.formBuilder.group({
      taskId: data.id,
      taskTitle: data.title,
      taskDescription: data.description,
      taskCreated: data.created
    });
  }


  ngOnInit(): void {
  }

  close() {
    this.dialog.closeAll();
  }

  update(dataForUp: any) {
    const task = new Task();
    task.id = dataForUp.taskId;
    task.title = dataForUp.taskTitle;
    task.description = dataForUp.taskDescription;
    this.taskService.update(task);
    this.close();
  }
}
