import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {User} from './model/user.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  allUsers(): Observable<User[]> {
    console.log('USER FIND ALL');
    return this.http.get<User[]>('https://taskcontroller14.herokuapp.com/api/v1/users/all');
  }


}
